﻿//      )                             *     
//   ( /(        *   )       (      (  `    
//   )\()) (   ` )  /( (     )\     )\))(   
//  ((_)\  )\   ( )(_)))\ ((((_)(  ((_)()\  
// __ ((_)((_) (_(_())((_) )\ _ )\ (_()((_) 
// \ \ / / (_) |_   _|| __|(_)_\(_)|  \/  | 
//  \ V /  | | _ | |  | _|  / _ \  | |\/| | 
//   |_|   |_|(_)|_|  |___|/_/ \_\ |_|  |_| 
// 
// This file is subject to the terms and conditions defined in
// file 'License.txt', which is part of this source code package.
// 
// Copyright © Yi.TEAM. All rights reserved.
// -------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Security.Cryptography;
using System.Text;
using Yisoft.Framework.Extensions;

namespace Yisoft.Framework.Utilities
{
	public class TokenHelper
	{
		public const int Version = 1;

		private const int _VERSION_LENGTH = 7;
		private const int _VERSION_POS_START = 3;
		private const int _VERSION_POS_END = 12;

		private const int _SALT_LENGTH = 8;
		private const int _SALT_POS_START = 13;
		private const int _SALT_POS_END = 24;

		private const int _DATA_MAXLENGTH = 32;
		private const int _DATA_POS_START = 25;
		private const int _DATA_POS_END = 61;

		private const int _TIMESTAMP_LENGTH = 8;

		private const string _RANDOM_SEEDS = "0123456789abcdef";
		private static readonly Dictionary<char, char> _Backpackage = new Dictionary<char, char>();
		private static readonly Dictionary<char, char> _BackpackageR = new Dictionary<char, char>();
		private static readonly Random _Random = new Random();

		/*
		 * 格式说明
		 * 0        8        16       24       32       40       48       56
		 * 01234567 01234567 01234567 01234567 01234567 01234567 01234567 01234567
		 * e4c74548 db5b5506 402153ef c1647091 81075c84 f333b0a3 d5099512 2127f7d6
		 * - total length: 64
		 * -      version:  3 <= pos < 10
		 * -         salt: 11 <= pos < 24
		 * -         data: 25 <= pos < 55
		 * -    timestamp: 56 <= pos < 64
		 */

		static TokenHelper()
		{
			// 背包字典初始化
			_Backpackage.Add('0', '9');
			_Backpackage.Add('1', '2');
			_Backpackage.Add('2', 'e');
			_Backpackage.Add('3', '3');
			_Backpackage.Add('4', '4');
			_Backpackage.Add('5', '7');
			_Backpackage.Add('6', '8');
			_Backpackage.Add('7', 'c');
			_Backpackage.Add('8', 'a');
			_Backpackage.Add('9', 'b');
			_Backpackage.Add('a', '0');
			_Backpackage.Add('b', '1');
			_Backpackage.Add('c', '5');
			_Backpackage.Add('d', '6');
			_Backpackage.Add('e', 'd');
			_Backpackage.Add('f', 'f');

			foreach (var pair in _Backpackage) _BackpackageR.Add(pair.Value, pair.Key);
		}

		private static string _BackpackEncode(string input)
		{
			var result = new StringBuilder();

			foreach (var c in input) result.Append(_Backpackage[c]);

			return result.ToString();
		}

		private static string _BackpackDecode(string input)
		{
			var result = new StringBuilder();

			foreach (var c in input) result.Append(_BackpackageR[c]);

			return result.ToString();
		}

		public static string Encrypt(string input, string key, string data = null)
		{
			var versionPosition = _Random.Next(_VERSION_POS_START, _VERSION_POS_END - _VERSION_LENGTH);
			var saltPosition = _Random.Next(_SALT_POS_START, _SALT_POS_END - _SALT_LENGTH);
			var dataPosition = _Random.Next(_DATA_POS_START, _DATA_POS_END - _DATA_MAXLENGTH);
			var timestamp = DateTime.Now.ToTimestamp();
			var salt = new SaltInfo(_GenerateSalt(), data, timestamp, versionPosition, saltPosition, dataPosition, data?.Length ?? 0);

			return Encrypt(input, key, salt);
		}

		public static string Encrypt(string input, string key, SaltInfo salt)
		{
			var keyBuffer = Encoding.UTF8.GetBytes(key + salt.Salt);

			using (var sha = new HMACSHA256(keyBuffer))
			{
				var buffer = sha.ComputeHash(Encoding.UTF8.GetBytes(input));
				var token = BitConverter.ToString(buffer).Replace("-", string.Empty).ToLower();

				return _AddSalt(token, salt);
			}
		}

		public static SaltInfo GetSalt(string token)
		{
			var decoded = _BackpackDecode(token);
			var versionPosition = Convert.ToInt32(decoded[2].ToString());
			var saltPosition = Convert.ToInt32($"{decoded[versionPosition + 1]}{decoded[versionPosition + 2]}");
			var dataPosition = Convert.ToInt32($"{decoded[versionPosition + 3]}{decoded[versionPosition + 4]}");
			var dataLength = Convert.ToInt32($"{decoded[versionPosition + 5]}{decoded[versionPosition + 6]}");
			var salt = decoded.Substring(saltPosition, _SALT_LENGTH);
			var data = decoded.Substring(dataPosition, dataLength);
			var timestamp = decoded.Substring(dataPosition + _DATA_MAXLENGTH - _TIMESTAMP_LENGTH, _TIMESTAMP_LENGTH);

			return new SaltInfo(salt, data, int.Parse(timestamp, NumberStyles.HexNumber), versionPosition, saltPosition, dataPosition, dataLength);
		}

		private static string _GenerateSalt()
		{
			var salt = new StringBuilder();

			while (salt.Length < _SALT_LENGTH)
			{
				var index = _Random.Next(_RANDOM_SEEDS.Length);

				salt.Append(_RANDOM_SEEDS[index]);
			}

			return salt.ToString();
		}

		private static string _AddSalt(string token, SaltInfo salt)
		{
			token = _Replace(token, 2, salt.VersionPosition.ToString());

			// add version
			token = _SetVersion(token, salt.VersionPosition);

			// add salt
			token = _Replace(token, salt.VersionPosition + 1, salt.SaltPosition.ToString("D2"));
			token = _Replace(token, salt.SaltPosition, salt.Salt);

			// add data and timestamp
			token = _Replace(token, salt.VersionPosition + 5, salt.DataLength.ToString("D2"));
			token = _Replace(token, salt.VersionPosition + 3, salt.DataPosition.ToString("D2"));

			if (!string.IsNullOrEmpty(salt.Data))
			{
				if (salt.Data.Length > _DATA_MAXLENGTH - _TIMESTAMP_LENGTH)
				{
					var message = $"data length is not in range, max length is {_DATA_MAXLENGTH - _TIMESTAMP_LENGTH}";

					throw new ArgumentOutOfRangeException(nameof(salt.Data), message);
				}

				token = _Replace(token, salt.DataPosition, salt.Data);
			}

			token = _Replace(token, salt.DataPosition + _DATA_MAXLENGTH - _TIMESTAMP_LENGTH, salt.Timestamp.ToString("x8"));

			// apply backpack
			return _BackpackEncode(token);
		}

		private static string _SetVersion(string token, int position) { return _Replace(token, position, Version.ToString()); }

		private static string _Replace(string input, int position, string value)
		{
			input = input.Remove(position, value.Length);

			return input.Insert(position, value);
		}

		public class SaltInfo
		{
			internal SaltInfo(string salt, string data, int timestamp, int versionPosition, int saltPosition, int dataPosition, int dataLength)
			{
				Salt = salt;
				Data = data;
				Timestamp = timestamp;
				VersionPosition = versionPosition;
				SaltPosition = saltPosition;
				DataPosition = dataPosition;
				DataLength = dataLength;
			}

			public string Salt { get; }

			public string Data { get; }

			public int Timestamp { get; }

			public int VersionPosition { get; }

			public int SaltPosition { get; }

			public int DataPosition { get; }

			public int DataLength { get; }
		}
	}
}

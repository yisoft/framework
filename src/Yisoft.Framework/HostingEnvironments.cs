﻿//      )                             *     
//   ( /(        *   )       (      (  `    
//   )\()) (   ` )  /( (     )\     )\))(   
//  ((_)\  )\   ( )(_)))\ ((((_)(  ((_)()\  
// __ ((_)((_) (_(_())((_) )\ _ )\ (_()((_) 
// \ \ / / (_) |_   _|| __|(_)_\(_)|  \/  | 
//  \ V /  | | _ | |  | _|  / _ \  | |\/| | 
//   |_|   |_|(_)|_|  |___|/_/ \_\ |_|  |_| 
// 
// This file is subject to the terms and conditions defined in
// file 'License.txt', which is part of this source code package.
// 
// Copyright © Yi.TEAM. All rights reserved.
// -------------------------------------------------------------------------------

namespace Yisoft.Framework
{
	public static class HostingEnvironments
	{
		public const string Development = "development";

		public const string Test = "test";

		public const string Staging = "staging";

		public const string Production = "production";

		public static string NormalizeEnvName(string envName, string defaultValue = Development)
		{
			if (string.IsNullOrWhiteSpace(envName)) return defaultValue ?? string.Empty;

			return envName.ToLower();
		}
	}
}

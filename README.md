# Yisoft.Framework

dotnet convenient development framework.

| build   | appveyor |
| -------:| :------: |
| release | [![Build status](https://ci.appveyor.com/api/projects/status/mfklwo3lor8mf8w9?svg=true)](https://ci.appveyor.com/project/ymind/framework-dh77n) |
| beta    | [![Build status](https://ci.appveyor.com/api/projects/status/8vtayiugd93pc02p?svg=true)](https://ci.appveyor.com/project/ymind/framework-5io65) |
| alpha   | [![Build status](https://ci.appveyor.com/api/projects/status/72ehkpduiq36soo7?svg=true)](https://ci.appveyor.com/project/ymind/framework) |


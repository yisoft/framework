﻿//      )                             *     
//   ( /(        *   )       (      (  `    
//   )\()) (   ` )  /( (     )\     )\))(   
//  ((_)\  )\   ( )(_)))\ ((((_)(  ((_)()\  
// __ ((_)((_) (_(_())((_) )\ _ )\ (_()((_) 
// \ \ / / (_) |_   _|| __|(_)_\(_)|  \/  | 
//  \ V /  | | _ | |  | _|  / _ \  | |\/| | 
//   |_|   |_|(_)|_|  |___|/_/ \_\ |_|  |_| 
// 
// This file is subject to the terms and conditions defined in
// file 'License.txt', which is part of this source code package.
// 
// Copyright © Yi.TEAM. All rights reserved.
// -------------------------------------------------------------------------------

using Xunit;
using Xunit.Abstractions;
using Yisoft.Framework.Extensions;
using Yisoft.Framework.Utilities;

namespace Yisoft.Framework.XUnitTest
{
	public class TokenHelperUnitTest
	{
		private readonly ITestOutputHelper _output;

		public TokenHelperUnitTest(ITestOutputHelper output) { _output = output; }

		[Theory]
		[InlineData("123456", "key", "0123456789abcdef")]
		public void ChineseZodiacTest(string input, string key, string data)
		{
			for (var i = 0; i < 500; i++)
			{
				var x = TokenHelper.Encrypt(input, key, data);
				var y = TokenHelper.GetSalt(x);
				var z = TokenHelper.Encrypt(input, key, y);

				_output.WriteLine($"{x}");
				//_output.WriteLine($"{y.ToJson()}");

				Assert.True(y.Data == data);
				Assert.True(x == z);
			}
		}
	}
}
